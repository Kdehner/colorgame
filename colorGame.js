var difficulty = 6;
var colors;
var pickedColor;

var squares = document.querySelectorAll(".square");
var colorDisplay = document.querySelector("#colorDisplay");
var messageDisplay = document.querySelector("#message");
var h1 = document.querySelector("h1");
var resetButton = document.querySelector("#reset");
var modeButtons = document.querySelectorAll(".mode");

init();

function init(){
    //Create event Listeners for mode buttons.
    setupModeButtons();
    //Create event listener for reset button
    resetButton.addEventListener("click", function(){
        reset();
    });
    //Create event listeners for squares
    setupSquares();
    //Call reset to set game space
    reset();
}

function setupModeButtons(){
    for (var i = 0; i < modeButtons.length; i++){
        modeButtons[i].addEventListener("click", function(){
            modeButtons[0].classList.remove("selected")        
            modeButtons[1].classList.remove("selected")        
            this.classList.add("selected");
    
            //Set Difficulty
            this.textContent ==="Easy" ? difficulty = 3: difficulty = 6;
            //Call game reset
            reset();
        });
    }
}

function setupSquares(){
    for (var i = 0; i < squares.length; i++) {
        //Add click events to squares
        squares[i].addEventListener("click", function(){
            //Get color of clicked square
            var squareColor = this.style.backgroundColor;
            //Compare color of clicked square to picked color
            if (squareColor === pickedColor) {
                messageDisplay.textContent = "Correct!"
                resetButton.textContent = "Play Again?";
                h1.style.backgroundColor = pickedColor;
                changeColors(squareColor);
            } else {
                this.style.backgroundColor = "#232323"
                messageDisplay.textContent = "Try Again!"
            }
        });
    }
}

function reset(){
    //Generate new colors
    colors = generateRandomColors(difficulty);
    //Pick new color
    pickedColor = pickColor();
    //Changed displayed color 
    colorDisplay.textContent = pickedColor;
    //Reset message
    messageDisplay.textContent = "";
    //Reset button text
    resetButton.textContent = "new colors"
    //Reset h1 background
    h1.style.backgroundColor = "steelblue";
    //Change square colors
    for (var i = 0; i < squares.length; i++) {
        if(colors[i]){
            squares[i].style.display = "block";
            squares[i].style.backgroundColor = colors[i];
        } else {
            squares[i].style.display = "none";
        }
        
    }
}

function changeColors(color) {
    //loop through all the colors
    for (var i = 0; i < squares.length; i++) {
        squares[i].style.backgroundColor = color;
    }
    //change colors to match givin color
}

function pickColor(){
    var random = Math.floor(Math.random() * colors.length);
    return colors[random];
}

function generateRandomColors(num) {
    //make array
    var arr = [];
    //addd num random colors to array
    for (var i = 0; i < num; i++) {
        //getRandom color and push into array
        arr.push(randomColor());
    }
    //return array
    return arr;
}

function randomColor(){
    //pick red from 0-255
    var r = Math.floor(Math.random() * 256);
    //pick green from 0-255
    var g = Math.floor(Math.random() * 256);
    //pick blue from 0-255
    var b = Math.floor(Math.random() * 256);
    return "rgb(" + r + ", " + g + ", " + b + ")"
}